use std::io;

fn clamscan() {
    use std::io::{self, Write};
    use std::process::Command;
    println!("Give me a Dir to Scan\n");
    let mut dir_scan = String::new();
    io::stdin()
        .read_line(&mut dir_scan)
        .expect("Failed to read path");
    let dir_scan_trim = dir_scan.trim();

    let output = Command::new("clamscan")
        .arg(dir_scan_trim)
        //.arg("--recursive")
        .output()
        .expect("failed to execute process");
    println!("Scanning!");
    println!("status: {}", output.status);
    io::stdout().write_all(&output.stdout).unwrap();
    io::stderr().write_all(&output.stderr).unwrap();
    println!("Aaaaand I'm done.")
    //assert!(output.status.success());
}
fn av_menu() {
    let mut av_task = String::new();
    io::stdin()
        .read_line(&mut av_task)
        .expect("Failed to read path");
    let av_task = "Update";
    match av_task {
        Scan => clamscan(),
        Update => freshclam(),
        //3 => return(0),
    }
}
fn freshclam() {
    use std::io::{self, Write};
    use std::process::Command;
    let output = Command::new("clamscan")
        .output()
        .expect("Failed to execute process");
    println!("Updating!");
}

fn main() {
    println!("Welcome!  What would you like to do?\n 1. Update AV\n 2. Scan a path.\n 3. Exit");

    av_menu();

    }


